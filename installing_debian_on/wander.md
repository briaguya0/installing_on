## Thinkpad X13 Gen 2a 20XH002NUS
| Manufacturer Support Product Home | [lenovo](https://pcsupport.lenovo.com/us/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x13-gen-2-type-20xh-20xj/20xh/20xh002nus) | [wayback](https://web.archive.org/web/20211004012432/https://pcsupport.lenovo.com/us/en/products/laptops-and-netbooks/thinkpad-x-series-laptops/thinkpad-x13-gen-2-type-20xh-20xj/20xh/20xh002nus) |
| - | - | - |

### UEFI Updates
available via [fwupd](https://packages.debian.org/sid/fwupd)

### UEFI Config
* in config/power ensure **Sleep State** is set to **Linux**
* turn off **Secure Boot**

# installing debian

### initial install
1. boot to your debian install usb (i used **firmware-11.0.0-amd64-DVD-1** from [here](https://cdimage.debian.org/images/unofficial/non-free/images-including-firmware/11.0.0+nonfree/amd64/))
2. if you don't have usb ethernet/tethering, then network config will fail. that's cool, we can do this part offline
3. partition the drive(s), i went for "Guided - use entire disk and set up encrypted LVM" (then deleted the LVs and made new ones for bigger swap)
4. skip the network mirror because we're going to switch to [sid](https://www.debian.org/releases/sid/) anyways
5. **boot into installed debian**
6. give yourself sudo
```
$ su -
# usermod -aG sudo username
```
7. get the sudoers file to update (rebooting works)
8. get temporary internet access (usb tethering from android worked for me)
9. switch to sid repos
```
$ sudo apt edit-sources
$ cat /etc/apt/sources.list
deb http://deb.debian.org/debian/ sid main non-free contrib
deb-src http://deb.debian.org/debian/ sid main non-free contrib
```
10. install sid
```
$ sudo apt update
$ sudo apt full-upgrade
```
11. reboot to complete upgrade to sid

### getting wifi working

this thing has a Realtek 8852 in it, so we're going to need to use [rtw89](https://github.com/lwfinger/rtw89). as of writing, it's not in the repos, but there's an [rfp](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=995598). for now, get a temporary internet connection running and

```
$ sudo apt install make gcc linux-headers-$(uname -r) build-essential git
$ git clone https://github.com/lwfinger/rtw89.git
$ cd rtw89
$ make
$ sudo make install
$ sudo modprobe rtw89pci
```

### testing hibernation
quick test with `systemctl hibernate`, if this fails check swap size

### fingerprint reader
this thing uses a **Goodix MOC Fingerprint Sensor (27c6:6594)**, which is [listed as a supported device](https://fprint.freedesktop.org/supported-devices.html) for [fprintd](https://tracker.debian.org/pkg/fprintd).

as of version 1.94 of fprintd and gnome 41.3 the reader seems to disconnect when trying to enroll new fingerprints

### trackpad
swipe to nav isn't working in firefox https://bugzilla.mozilla.org/show_bug.cgi?id=1539730

### keyboard lights
the mute mic lights up white when muted, no light when not muted
